function log(message, method) {
  method = method || 'log'

  if (ENV === 'development') {
    console[method](message)
  }
}

function info(message) {
  log(message, 'info')
}

function error(message) {
  log(message, 'error')
}

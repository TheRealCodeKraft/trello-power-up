/**
 * Build Trello Api Url
 */
function buildApiUrl(route, params ) {
  //REFACTOR use https://devdocs.io/javascript/global_objects/encodeuricomponent and do not use 2 ways to build string (concat and template)
  let buildUrl = `https://api.trello.com/1/${route}?`
  buildUrl += Object.keys(params).map(key => key + '=' + params[key]).join('&') + '&key=' + KEY

  return buildUrl
}

/**
 * Call Api Trello
 */
 function call(method, endpoint , params){
   return new Promise(function(resolve, reject) {
     const xhr = new XMLHttpRequest()
     xhr.addEventListener("loadend", function() {
       resolve(JSON.parse(this.responseText))
     })
     xhr.open(`${method}`, buildApiUrl(endpoint, params))
     xhr.send()
   })
 }

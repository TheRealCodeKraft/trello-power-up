const Promise = window.TrelloPowerUp.Promise

/*******************************/
/* Init PowerUp initialization */
/*******************************/

window.TrelloPowerUp.initialize({
  'on-enable': onEnable,
  'on-disable': onDisable,
  'card-buttons': buildButtons([ buildPublishButton, buildUpdateButton ]),
  'board-buttons': buildButtons([ buildMainBoardButton ]),
  'remove-data': onRemoveData,
  'authorization-status': function(t) {
    return t.loadSecret('token')
    .then(token => {
      return { authorized: Boolean(token) }
    })
  },
  'show-authorization': function(t, options) {
    return t.popup({
      title: 'Autoriser votre compte',
      url: '/views/auth.html',
      height: 100,
    })
  },
  'show-settings': function(t, options) {
    return t.modal({
      title: 'Configuration du Power-Up CodeKraft Dev Process',
      url: '/views/configure.html',
      height: 100
    })
  }
})

/**
 * On enable callback
 */
function onEnable(t) {
  return t.modal({
    url: '/views/on-enable.fr.html',
    height: 100,
    title: 'CodeKraft dev process Power-Up'
  })
}

/**
 * On disable callback
 */
async function onDisable(t) {
  await onRemoveData(t)
  await t.clearSecret('token')
}

/**
 * On remove-data callback
 */
async function onRemoveData(t) {
  await t.remove('board', 'shared', [
    'done-list',
    'release-list',
    'published-list'
  ])
}

/**
 * Build buttons to attach to PowerUp capabilities
 */
function buildButtons(builders) {
  return function(t, opts) {
    if (opts.context.permissions.board !== 'write') {
      return []
    }

    return Promise.all(builders.map(builder => builder(t, opts))).then(values => {
      let buttons = []

      values.forEach(value => {
        buttons = buttons.concat(value)
      })
      return buttons
    })
  }
}

/**
 * Build main CodeKraft board button
 */
function buildMainBoardButton(t, opts) {
  return [{
    icon: {
      dark: ICON_WHITE,
      light: ICON_BLACK
    },
    text: 'CodeKraft',
    callback: async function(context) {
      const [ trello, token, lists ] = await getTokenAndLists(t)

      if (!token) {
        return context.popup({
          title: 'Autoriser votre compte',
          url: '/views/auth.html',
          height: 100
        })
      }

      const items = []
      return getTokenAndLists(t)
        .then(([ t, token, lists ]) => {
          if (lists.length < 3) {
            items.push(
              {
                text: 'Configuration',
                callback: initialize
              },
            )
            return false
          }

          return true
        })
        .then(function(ready) {
          if (!ready) {
            return ready
          }

          return getLists(t)
          .then(async function(configuredLists) {
            if (configuredLists.length !== 3) {
              return ready
            }

            const releaseList = await findList(t, lists, LIST_RELEASES)
            const publishedList = await findList(t, lists, LIST_PUBLISHED)
            const doneList = await findList(t, lists, LIST_DONE)

            if (
              releaseList.cards.length === 0
              && publishedList.cards.length === 0
              && doneList.cards.length === 0
            ) {
              return ready
            }

            items.push(
              {
                text: 'Ajouter une release',
                callback: createRelease
              }
            )

            return ready
          })
        })
        .then((ready) => {
          if (!ready) {
            return ready
          }

          items.push(
            {
              text: 'Configuration',
              callback: configure
            },
          )

          return ready
        })
        .then(() => {
          return context.popup({
            title: "CodeKraft",
            items
          })
        })
        .catch(error => {
          t.alert({
            message: `Une erreur est survenue, nous sommes obligés de supprimer la configuration du Power-Up.`,
            duration: 5,
            display: 'error'
          })
          onRemoveData(t)
        })
    }
  }]
}

/**
 * Build publish card button
 */
function buildPublishButton(t, opts) {
  return Promise.all([
    t.loadSecret('token'),
    t.lists('all'),
    t.list('name', 'cards'),
    t.card('id'),
  ])
  .then(async function ([ token, lists, list, card ]) {

    const listReleases = await findList(t, lists, LIST_RELEASES)

    if (
      // Display button only on release list cards
      list.name !== listReleases.name

      // Do not display button if there is only one card (info card)
      || list.cards.length < 2

      // Do not display button if card id is not same as last release card
      || card.id !== list.cards[1].id
    ) {
      return []
    }

    return [{
      icon: ICON_PUBLISH,
      text: 'Publier',
      callback: publish,
    }]
  })
}

/**
  * Build update checklist button
  */
async function buildUpdateButton(t, opts) {
  return Promise.all([
    t.loadSecret('token'),
    t.lists('all'),
    t.list('name', 'cards'),
    t.card('id', 'badges')
  ])
  .then(async function ([ token,lists, list, card ]) {
    const listDone = await findList(t, lists, LIST_DONE)
    const listReleases = await findList(t, lists, LIST_RELEASES)
    const cardsDoneNumber = listDone.cards.length - 1
    const checkItemsCardNumber = card.badges.checkItems

    if(
      // Do not display button if the checklist is up to date
      cardsDoneNumber === checkItemsCardNumber

      //  Do not display button if there is no card in done list
      || cardsDoneNumber === 0

      // Display button only on release list cards
      || list.name !== listReleases.name

      // Do not display button if there is only one card (info card)
      || list.cards.length < 2

      // Do not display button if card id is not same as last release card
      || card.id !== list.cards[1].id
    ) {
      return []
    }

    return [{
      icon: ICON_UPDATE,
      text: 'Mettre à jour',
      callback: update,
    }]
  })
}

/*************/
/* Callbacks */
/*************/

/**
  * Update callback
  */
async function update(t) {
  return getTokenAndLists(t)
  .then(async function([ t, token, lists ]) {
    const listDone = await findList(t, lists, LIST_DONE)
    const listReleases = await findList(t, lists, LIST_RELEASES)
    const cardsListDone = listDone.cards
    const firstCardReleases = listReleases.cards[1]

    return call('GET', `cards/${firstCardReleases.id}/checklists`, {
      checkItems: 'all',
      checkItem_fields: 'name',
      filter: 'all',
      fields: 'none',
      token,
    })
    .then(cardChecklists => {
      return addCardAtChecklist(token, cardChecklists, cardsListDone)
    })
  })
}

function addCardAtChecklist(token, checklistCard, cardsListDone) {
  const idChecklist = checklistCard[0].id
  const itemsChecklist = checklistCard[0].checkItems
  const itemName = []
  const urlCard = []

  // Put all the name's items of the checklist in the Releases Card in an arrray
  itemsChecklist.forEach((item, i) => {
    itemName.splice(i, 0, item.name)
  })

  // Put all the url's cards  of the Done List in an array
  cardsListDone.slice(1).forEach((card, i ) => {
    urlCard.splice(i, 0, card.url)
  })

  // Check if the cards of the Done List are already in the checklist
  itemName.forEach((item, iItem) => {
    urlCard.forEach((url, iUrl) => {
      // If the cards of the Done List are in, splice it to only have the cards who are not yet in the checklist
      if (url === item){
        urlCard.splice(iUrl, 1)
      }
    })
  })

  if (urlCard.length < 0){
    return
  }

  const tasks = urlCard.map(url => (callback) => {
    updateCard(url, token, idChecklist)
    .then(() => callback())
    .catch(callback)
  })
  return new Promise((resolve, reject) => {
    async.series(tasks, (error, data) => {
      if (error) {
        reject(error)
        return
      }
      resolve()
    })
  })
}

function updateCard(url, token, idChecklist) {
  return call('POST', `checklists/${idChecklist}/checkItems`, {
    name: url,
    pos: 'bottom',
    checked: 'false',
    token,
  })
  .then(updateCard => {
    return updateCard
  })
}

/**
 * Publish callback
 */
function publish(t){
  return Promise.all([
    Promise.resolve(t),
    t.loadSecret('token'),
    t.lists('all'),
    t.card('id'),
  ])
  .then(findFirstCard)
  .then(recoverChecklistCard)
  .then(publishCards)
  .then(addComment)
  .then(addSeparatingCard)
  .then(tada)
  .then(() => {
    t.hideCard()
  })
  .catch(error => {
    if (!error.code) {
      console.error(error)
      return
    }

    log(error)
  })
}

async function findFirstCard([ t, token, lists, card]) {
 const publishedList = await findList(t, lists, LIST_PUBLISHED)
 const firstCard = publishedList.cards[0]

 if (publishedList.cards.length === 0) {
   t.hideCard()
   t.alert({
     message: 'La colonne de publication doit comporter au moins une carte d\'information.',
     duration: 30,
     display: 'error'
   })

   const error = new Error('Published list should have at least one info card')
   error.code = 1
   throw error
 }

 return call(`GET`, `cards/${firstCard.id}`, {
   token,
 })
 .then(firstCard => [ t, token, lists, card, firstCard ])
}

async function recoverChecklistCard([ t, token, lists, card, firstCard ]) {
  const listPublished = await findList(t, lists, LIST_PUBLISHED)
  const listDone = await findList(t, lists, LIST_DONE)
  const listReleases = await findList(t, lists, LIST_RELEASES)

  if (listDone.cards.length -1 === 0) {
    t.alert({
      message: 'La publication à déjà été faite !',
      duration: 5,
      display: 'info'
    })
    return []
  }

  return call('GET', `cards/${card.id}/checklists`, {
    checkItems: 'all',
    checkItem_fields: 'name',
    filter: 'all',
    fields: 'none',
    token,
  })
  .then(checklistCards => [ t, checklistCards, token, listDone, listPublished, listReleases, card, firstCard ])
}

function publishCards([ t, checklistCards, token, listDone, listPublished, listReleases, card, firstCard ]) {
  const doneCards = listDone.cards.slice(1)
  const itemsChecklist = checklistCards[0].checkItems

  const tasks = doneCards.reverse().map(doneCard => (callback) => {
    moveDoneCards(token, listPublished, doneCard, itemsChecklist, firstCard)
    .then(() => callback())
    .catch(callback)
  })

  return new Promise((resolve, reject) => {
    async.series(tasks, (error, data) => {
      if (error) {
        reject(error)
        return
      }
      resolve()
    })
  })
  .then(publish => [ t, token, card, listPublished, listReleases, firstCard ])
}

function moveDoneCards(token, list, doneCard, itemsChecklist, firstCard) {

  const item = itemsChecklist.find((item, i) => {
    if(item.name !== decodeURIComponent(doneCard.url)){
      return false
    }

    return true
  })

  if (!item) {
    console.error(`Done card not found ${doneCard.url}`)
    return Promise.resolve()
  }

  return call('PUT', `cards/${doneCard.id}`, {
    idList: list.id,
    pos: firstCard.pos + 0.0001,
    token,
  })
  .then( doneCardMove => {
    return doneCardMove
  })
}

function addComment([ t, token, card, listPublished, listReleases, firstCard ]) {
  const checkItemsCardReleasesLength = listReleases.cards[1].badges.checkItems

  let plural = ""
  if (checkItemsCardReleasesLength > 1) {
    plural = "s"
  }

 return call('POST', `cards/${card.id}/actions/comments`, {
   text: `Publication de ${checkItemsCardReleasesLength} carte${plural} faite ! :tada:`,
   token,
 })
 .then(comment => {
   return [ t, token, firstCard, listPublished ]
 })
}

function addSeparatingCard([ t, token, firstCard, listPublished ]) {
  const date = new Date()
  const actualDate = String(date.getDate()).padStart(2, '0') + "/" + String(date.getMonth() + 1).padStart(2, '0') + "/" + date.getFullYear()
  const cardName = "Publié le " + actualDate + " "

  return call('POST', `cards`, {
    name: cardName,
    pos: firstCard.pos + 0.0001,
    idList: listPublished.id,
    token,
  })
  //.then(card => {
    //return call('POST', `cards/${card.id}/stickers`, {
      //image: 'check',
      //top: -30,
      //left: -30,
      //zIndex: 0,
      //rotate: 20
    //})
  //})
  .then(() => { return [ t ] })
}

function tada([ t ]) {
  t.alert({
    message: 'Good job, la publication a été faite !',
    duration: 5,
    display: 'sucess'
  })
}

/**
 * Initialize
 */
function initialize(t) {
  return t.modal({
    url: '/views/initialize.html',
    height: 100,
    title: language.initialize.title
  })
}

/**
 * Configure
 */
function configure(t) {
  return t.modal({
    url: '/views/configure.html',
    height: 100,
    title: language.configuration.title
  })
}

/**
 * Create release callback
 */
function createRelease(t) {
  return getTokenAndLists(t)
    .then(findFirstCardPos)
    .then(createCard)
    .then(createCheckList)
    .then(linkDoneCardsToCheckList)
    .then(function() {
      t.closePopup()
    })
    .catch(error => {
      if (!error.code) {
        console.error(error)
        return
      }

      log(error)
    })
}

async function findFirstCardPos([ t, token, lists ]) {
  const releasesList = await findList(t, lists, LIST_RELEASES)
  const firstCard = releasesList.cards[0]

  if (releasesList.cards.length === 0) {
    t.alert({
      message: language.errors.releaseInfoCard,
      duration: 30,
      display: 'error'
    })

    const error = new Error('Releases list should have at least one info card')
    error.code = 1
    throw error
  }

  return call(`GET`, `cards/${firstCard.id}`, {
    token,
  })
  .then(firstCard => [ t, token, lists, firstCard ])
}

async function createCard([ t, token, lists, firstCard ]) {
  info('Create card')

  const releasesList = await findList(t, lists, LIST_RELEASES)
  const doneList = await findList(t, lists, LIST_DONE)
  const date = new Date()
  const dateCard = "[" + String(date.getDate()).padStart(2, '0') + "/" + String(date.getMonth() + 1).padStart(2, '0') + "/" + date.getFullYear() + "]"

  return call('POST', `cards`, {
    name: dateCard,
    idList: releasesList.id,
    keepFromSource: 'all',
    pos: firstCard.pos + 0.0001,
    token,
  })
  .then(function(card) {
    return [ token, card, doneList, releasesList ]
  })
}

function createCheckList([ token, card, doneList ]) {
  info('Create check list')

  return call('POST', `checklists`, {
    idCard: `${card.id}`,
    name: 'Card List',
    token,
  })
  .then(function(checkList) {
    return [ token, checkList, doneList ]
  })
}

function linkDoneCardsToCheckList([ token, checkList, doneList ]) {
  info('Link cards to check list')

  const cards = doneList.cards.slice(1)
  const tasks = cards.map(card => (callback) => {
    buildCardLinkPromise(card, token, checkList.id)
    .then(() => callback())
    .catch(callback)
  })

  return new Promise((resolve, reject) => {
    async.series(tasks, (error, data) => {
      if (error) {
        reject(error)
        return
      }
      resolve()
    })
  })
}

function buildCardLinkPromise(card, token, idChecklist) {
  const urlCard = card.url

  return call('POST', `checklists/${idChecklist}/checkItems`, {
    name: urlCard,
    pos : 'bottom',
    checked: 'false',
    token,
  })
}

/*******************/
/* Utils functions */
/*******************/

/**
 * Find a list by its name
 */
async function findList(t, lists, name) {
  const configuration = await t.get('board', 'shared', `${name}-list`)
  const list = lists.find(list => list.id === configuration)
  if (!list) {
    throw new Error(`List ${name} not found`)
  }

  return list
}

/**
 * Get token and lists info
 */
function getTokenAndLists(t) {
  return Promise.all([
    Promise.resolve(t),
    t.loadSecret('token'),
    t.lists('all')
  ])
}

/**
 * Get release and published list
 */
function getLists(t) {
  return Promise.all([
    t.get('board', 'shared', 'release-list'),
    t.get('board', 'shared', 'published-list'),
    t.get('board', 'shared', 'done-list'),
  ])
  .then(lists => lists.filter(list => typeof list !== 'undefined'))
}

/**
 * Sort cards by idShort
 */
function cardsSorter(aCard, bCard) {
  return aCard.idShort > bCard.idShort
}

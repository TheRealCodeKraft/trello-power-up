
const KEY = '55c40d3284f986cccbfe3e00124b6ba7'
const ENV = 'production'

const LIST_DONE = 'done'
const LIST_PUBLISHED = 'published'
const LIST_RELEASES = 'release'

/*
** LIBRAIRIE ICONS PIXEL-PERFECT = https://www.flaticon.com/authors/pixel-perfect/filled
*/

/*
** Logo Codekraft ?
** URL ICON POWER-UP = https://image.flaticon.com/icons/svg/1389/1389010.svg
*/

/*
** BOARD-BUTTON ICONS
*/
const ICON_WHITE = 'https://lh3.googleusercontent.com/ap6qiEI7x3_4embyPZRltlxfEWWXCLMRiSqIfbfDRsU_hKTImzoTzQpqMSecU1MBiBAOCv7rVtYaH_Wo6Z39sB0zQ_r1MCHK8c980ohf_VkFux25_Bh13V8hjKe3o6gjoibi_2l3VThJ4HfvB1ZgdVLd_ndxr4aiHbunKKjE1t5NeSzEPqcuLvrLgQt2FkAxvfd5GbBdePiO8_9u5N5_WG9121mf3j8bIkPZfhDI86R5QJwIl8VwbIZV-2-Yh35rHmOE2kyDVJAWyujHRkqG8iHtAV7lkoLbTjvCZB7HjeqL4MIE36eaL-4O8m1Lqjke2kKpPuc0-cciAJTL1aTTmSIopkI38t98nkZb2A2sKT6HOoSEfPxqOO_27S27WSBzzG5vYe8R6o1X6vKLt4Wm3TGdlcMXBdErNk0p9KqzTp6WhFyRagDZ5jEj5c7fDcd5KVkk5Ijg1hqU7eq4JDyTkxi_JxvVq3MBe4dwEZZ4vlxwpFtjLQZ2VSiPfja7xJW38tld-JnLiXVbltw-JD3LXid-0dXshopU3cRp841dzUUVYvQYWqAmSjvK_mM45PRZMqP0WMMU329UcXT1IeoiWrAVmD_YATqUSkZ_Ci29LgVthIyybPHgONvs9R18tbzoDeamO_FTScJi5YG9ZYQ1uebcMVIyNEZtT958GZQQq4fzvVIJf1nh=s114-no'
const ICON_BLACK = 'https://lh3.googleusercontent.com/5mFpB7rOiemDLtYDwaHQ2R85fQM11bX5ZG9qH6M2QWXNb7fKXJvVlCZfUU5wq3WAbEtjpqSQ4yDaEFg8qovY_OZ8oZOjJObfKFdZZOa4cYfE0wUnhqTYHSfs4Sq6R6Qosa6DQmgAaqITGUZKFdjfIlVfG7GllZFXzEb5BkbcS-4AA274ZRedTohniBKi9ul3MzQZxqXy5c10SOIm_QcfGVTZ487DtjhPNkY3LwxfnScfB4WmPjtixImHE3scq4ca9KKHjwerF2lVdmYhgj_UFXh6mPFYrWfNAp_ssz-c3vXkX6VWfk8KB4SlGORov3saRBu9A4UbaU7IILzBntBptQgS_8tMAn09Qu3Eyr-5tuRgm2mltOmvbJDMNlVxzB80nXrniARZFzZ8ZM-0B2uBFuR7UqvqY3zhBpn8JpTALasTH5VmscyGC2fF_PVYIw7Rlx8bMLWuYYupYnpCS_SPUMXFKJkDOzD-Vokgf6hVSle47MbCPW_tTH1y6JOxUsyZWXUy8MnJzmdsE1E7-Cu1fDP90vMzOZwL4_nYRdWBVQYogKQSWoD_Wm42fav8YXX42dRkcbdtyRiRcG-Be773fg3V5MMt42pBOM4NR_ilbNaXKJujYWXAVtOxUfRQ98f6Q47zqtDspFyk6dtDe9qOxU-vKQ9RAP0rNh8pjum-ewR0KN9ulXxq=s114-no'

/*
** ITEMS BOARD-BUTTON ICONS
*/
const ICON_CONFIG = 'https://image.flaticon.com/icons/svg/1827/1827708.svg'
const ICON_ADD_RELEASE = 'https://image.flaticon.com/icons/svg/1828/1828923.svg'
// ou 'https://image.flaticon.com/icons/svg/1828/1828919.svg'
// ou 'https://image.flaticon.com/icons/svg/1828/1828923.svg'

/*
** CARD-BUTTONS ICONS
*/
const ICON_PUBLISH = 'https://image.flaticon.com/icons/svg/724/724933.svg'
const ICON_UPDATE = 'https://image.flaticon.com/icons/svg/724/724974.svg'

const languages = {
  fr: {
    initialize: {
      title: 'Configuration du Power-Up',
    },
    configuration: {
      title: 'Configuration du Power-Up',
      saved: 'La configuration a bien été enregistrée',
      initializing: 'Initialisation en cours'
    },
    lists: {
      releases: 'Publications',
      published: 'Publiées',
      done: 'Faites'
    },
    cards: {
      releasesInfo: 'Cartes présentant les publications successives',
      publishedInfo: 'Cartes publiées (en lien avec une publication)',
      doneInfo: 'Cartes terminées en attente de publication'
    },
    errors: {
      releaseInfoCard: 'La colonne de publication doit comporter au moins une carte d\'information.',
    },
  },
  en: {
  }
}

let lang = 'fr'
const language = languages[lang]
